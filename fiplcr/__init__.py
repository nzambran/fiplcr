# -*- coding: utf-8 -*-

'''Functions to make UV spectroscopy data analysis for the Solar Corona
focusing on FIP bias determination'''

from .fip_map import fip_map
from .specline import *
from .linear_combination import *


