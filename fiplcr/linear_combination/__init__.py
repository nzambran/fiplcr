# -*- coding: utf-8 -*-

'''Linear combination of spectral lines'''

from .linear_comb import LinearComb

__all__ = ['LinearComb']
